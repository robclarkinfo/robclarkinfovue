import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
// Import Bootstrap-vue components
import { LayoutPlugin, CardPlugin, BIcon, NavbarPlugin, ButtonPlugin } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

// Sets up bootstrap Icons, Grid layout, and Card plugins for use
Vue.component("b-icon", BIcon);
Vue.use(LayoutPlugin);
Vue.use(CardPlugin);
Vue.use(NavbarPlugin);
Vue.use(ButtonPlugin);

Vue.config.productionTip = false;

// Creates the vue app
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
